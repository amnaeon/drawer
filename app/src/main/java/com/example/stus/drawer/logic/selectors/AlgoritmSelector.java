package com.example.stus.drawer.logic.selectors;


import com.example.stus.drawer.logic.algoritms.IAlgoritm;
import com.example.stus.drawer.logic.algoritms.LineAlgoritm;
import com.example.stus.drawer.logic.algoritms.TestAlgoritm;
import com.example.stus.drawer.logic.models.Line;
import com.example.stus.drawer.logic.models.Point;

import static com.example.stus.drawer.logic.Const.Algoritms.POLIGON_ALGORITM;
import static com.example.stus.drawer.logic.Const.Algoritms.LINE_ALGORITM;

public class AlgoritmSelector {
    public static IAlgoritm getAlgoritm(int type) {
        IAlgoritm currentAlgoritm = null;
        switch (type) {
            case LINE_ALGORITM:
                TestAlgoritm<Point, Point> testAlgoritm = new TestAlgoritm<>();
                currentAlgoritm = testAlgoritm;
                break;
            case POLIGON_ALGORITM:
                LineAlgoritm<Line, Line> lineAlgoritm = new LineAlgoritm<>();
                currentAlgoritm = lineAlgoritm;
                break;

        }
        return currentAlgoritm;
    }
}
