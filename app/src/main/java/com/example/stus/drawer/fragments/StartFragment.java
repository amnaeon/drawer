package com.example.stus.drawer.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.stus.drawer.R;
import com.example.stus.drawer.TestView;
import com.example.stus.drawer.logic.algoritms.IAlgoritm;
import com.example.stus.drawer.logic.drawers.IDraw;
import com.example.stus.drawer.logic.selectors.AlgoritmSelector;
import com.example.stus.drawer.logic.selectors.DrawerSelector;
import com.example.stus.drawer.logic.selectors.WorkerSelector;
import com.example.stus.drawer.logic.workers.IWork;

import static com.example.stus.drawer.logic.Const.Algoritms.LINE_ALGORITM;
import static com.example.stus.drawer.logic.Const.Algoritms.POLIGON_ALGORITM;
import static com.example.stus.drawer.logic.Const.Drawers.DRAG_DRAWER;
import static com.example.stus.drawer.logic.Const.Workers.ONE_STEP_WORKER;
import static com.example.stus.drawer.logic.Const.Workers.TWO_STEP_WORKER;


public class StartFragment extends BaseFragment implements View.OnClickListener{
    private Button drawLine;
    private Button drawPoligon;
    private TestView view;

    @Override
    protected int viewID() {
        return R.layout.canvas;
    }

    public static BaseFragment getInstance() {
        return new StartFragment();
    }

    @Override
    protected Initer getInit() {
        return super.getInit().add(this::init);
    }

    private void init() {
        drawLine = (Button) parent.findViewById(R.id.line);
        drawPoligon = (Button) parent.findViewById(R.id.poligon);
        view = (TestView)parent.findViewById(R.id.test_view);

        drawLine.setOnClickListener(this);
        drawPoligon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        IWork currentWorker;
        IAlgoritm currentAlgoritm;
        IDraw currentDrawer;
        int workerMode = 0;
        int algoritmMode = 0;
        int drawerMode = 0;

        switch (v.getId()){
            case R.id.line:
                workerMode = ONE_STEP_WORKER;
                algoritmMode = LINE_ALGORITM;
                drawerMode = DRAG_DRAWER;
                break;
            case R.id.poligon:
                workerMode = TWO_STEP_WORKER;
                algoritmMode = POLIGON_ALGORITM;
                break;
        }
        currentWorker = WorkerSelector.getSelector(workerMode);
        currentAlgoritm = AlgoritmSelector.getAlgoritm(algoritmMode);
        currentDrawer = DrawerSelector.getDrawer(drawerMode);
        view.changeMode(currentAlgoritm,currentWorker,currentDrawer);
    }
}
