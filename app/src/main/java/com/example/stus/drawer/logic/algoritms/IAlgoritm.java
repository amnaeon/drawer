package com.example.stus.drawer.logic.algoritms;

public interface IAlgoritm<T,P> {
    void prepare(T t);
    P getAlgoritmRes();
}
