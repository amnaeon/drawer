package com.example.stus.drawer.logic.drawers;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

import com.example.stus.drawer.logic.data_buffer.AlgoritmBuffer;
import com.example.stus.drawer.logic.models.Point;

public class DragDrawer implements IDraw {
    @Override
    public void draw(Canvas canvas, AlgoritmBuffer algoritmBuffer) {
        Path mPath = new Path();
        Paint mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.BLACK);
        mPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(15);
        for (int i = 0; i < algoritmBuffer.getAlgoritmDate().size() - 1; i++) {
            Point startPoint = (Point) algoritmBuffer.getAlgoritmDate().get(i);
            Point endPoint = (Point) algoritmBuffer.getAlgoritmDate().get(i + 1);
            float startX = startPoint.getX();
            float startY = startPoint.getY();
            float endX = endPoint.getX();
            float endY = endPoint.getY();
            mPath.moveTo(startX, startY);
            mPath.lineTo(endX, endY);
            canvas.drawPath(mPath,mPaint);
            mPath.reset();


        }
    }
}
