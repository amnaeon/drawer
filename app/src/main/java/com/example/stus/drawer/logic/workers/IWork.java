package com.example.stus.drawer.logic.workers;


import com.example.stus.drawer.logic.algoritms.IAlgoritm;
import com.example.stus.drawer.logic.models.BaseModel;

/**
 * Created by stus on 07.06.17.
 */
public interface IWork {
    IAlgoritm prepare(IAlgoritm iAlgoritm, BaseModel model);
    IAlgoritm move(IAlgoritm iAlgoritm,BaseModel model);
    IAlgoritm stop(IAlgoritm iAlgoritm,BaseModel model);
}
