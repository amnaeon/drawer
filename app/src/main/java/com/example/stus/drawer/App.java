package com.example.stus.drawer;

import android.app.Application;
import android.support.annotation.Nullable;

public class App extends Application {
    private static MainActivity mainActivity;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static void registerActivity(MainActivity mainActivity) {
        App.mainActivity = mainActivity;
    }

    @Nullable
    public static MainActivity getCurrentActivity() {
        return mainActivity;
    }


}
