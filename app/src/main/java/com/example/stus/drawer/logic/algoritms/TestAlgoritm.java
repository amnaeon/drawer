package com.example.stus.drawer.logic.algoritms;


import com.example.stus.drawer.logic.models.Point;

public class TestAlgoritm<I extends Point,O extends Point> implements IAlgoritm<I,O> {
    private Point p = new Point();

    @Override
    public void prepare(I i) {
        p = (Point) i;
    }

    @Override
    public O getAlgoritmRes() {
        return (O) p;
    }
}

