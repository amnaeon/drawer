package com.example.stus.drawer.logic.drawers;

import android.graphics.Canvas;

import com.example.stus.drawer.logic.data_buffer.AlgoritmBuffer;

public interface IDraw {
    public void draw(Canvas canvas, AlgoritmBuffer algoritmBuffer);
}
