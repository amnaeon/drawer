package com.example.stus.drawer;

import android.content.Context;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.example.stus.drawer.logic.BackStack;
import com.example.stus.drawer.logic.algoritms.IAlgoritm;
import com.example.stus.drawer.logic.data_buffer.AlgoritmBuffer;
import com.example.stus.drawer.logic.drawers.IDraw;
import com.example.stus.drawer.logic.models.BaseModel;
import com.example.stus.drawer.logic.workers.IWork;

/**
 * Created by stus on 27.06.17.
 */

public class TestView extends View {
    private IAlgoritm currentAlgoritm = null;
    private IWork currentWorker = null;
    private IDraw currentDrawer = null;
    private AlgoritmBuffer currentAlgoritmBuffer = null;

    public TestView(Context context) {
        super(context);
    }

    public TestView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TestView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (int i = 0; i < BackStack.getInstance().getOpperations().size(); i++) {
            AlgoritmBuffer algoritmBuffer = BackStack.getInstance().getOpperations().get(i);
            algoritmBuffer.getDrawer().draw(canvas, algoritmBuffer);
        }
        if(currentAlgoritmBuffer != null){
            currentAlgoritmBuffer.getDrawer().draw(canvas,currentAlgoritmBuffer);
        }
    }

    public void changeMode(IAlgoritm currentAlgoritm, IWork currentWorker, IDraw currentDrawer) {
        this.currentAlgoritm = currentAlgoritm;
        this.currentWorker = currentWorker;
        this.currentDrawer = currentDrawer;
        currentAlgoritmBuffer = new AlgoritmBuffer();
        currentAlgoritmBuffer.setDrawer(currentDrawer);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (currentAlgoritm != null && currentWorker != null) {
            BaseModel baseModel =  new BaseModel(event.getX(), event.getY());
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    currentAlgoritm = currentWorker.prepare(currentAlgoritm,baseModel);
                    currentAlgoritmBuffer.makeAction(currentAlgoritm);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    currentAlgoritm = currentWorker.move(currentAlgoritm,baseModel);
                    currentAlgoritmBuffer.makeAction(currentAlgoritm);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    currentAlgoritm = currentWorker.stop(currentAlgoritm,baseModel);
                    currentAlgoritmBuffer.makeAction(currentAlgoritm);
                    BackStack.getInstance().add(currentAlgoritmBuffer);
                    currentAlgoritmBuffer = new AlgoritmBuffer();
                    currentAlgoritmBuffer.setDrawer(currentDrawer);
                    invalidate();
                    break;

            }
        }
        return true;
    }
}
