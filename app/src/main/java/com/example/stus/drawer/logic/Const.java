package com.example.stus.drawer.logic;

public class Const {
    public class Algoritms {
        public final static int LINE_ALGORITM = 0;
        public final static int POLIGON_ALGORITM = 1;
    }

    public class Workers {
        public final static int ONE_STEP_WORKER = 0;
        public final static int TWO_STEP_WORKER = 1;

    }

    public class Drawers{
        public final static int DRAG_DRAWER = 0;
    }
}
