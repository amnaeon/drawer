package com.example.stus.drawer;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;


import com.example.stus.drawer.fragments.BaseFragment;
import com.example.stus.drawer.fragments.StartFragment;

import java.util.Stack;


/**
 * Created on 12.02.16.
 */
public class UiManager {
    private static UiManager ourInstance = new UiManager();
    private MainActivity parent;

    private Stack<BaseFragment> backStack = new Stack<>();

    public static UiManager getInstance() {
        return ourInstance;
    }

    private UiManager() {
    }

    public void init(MainActivity parent) {
        this.parent = parent;
        parent.setContentView(R.layout.activity_main);


    }

    public boolean back() {
        if (backStack.size() < 2) return true;
        if (backStack.lastElement().onBackPressed()) {
            backStack.pop();
            showFragment(backStack.pop());
            return false;
        }
        return true;
    }


    protected void showFragment(BaseFragment fragment) {
        FragmentManager manager = parent.getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();

        backStack.push(fragment);

        ft = manager.beginTransaction();
        ft.replace(R.id.main_container, fragment);
        if (!commitTransaction(ft)) return;
        if (manager == null) return;
        manager.executePendingTransactions();

    }

    private boolean commitTransaction(FragmentTransaction ft) {
        try {
            ft.commit();
        } catch (Exception e) {
            //todo log it
            try {
                ft.commitAllowingStateLoss();
            } catch (Exception e1) {
                //todo Log it
                return false;
            }
        }
        return true;
    }


    public static void showStartFragment() {
        getInstance().showFragment(StartFragment.getInstance());
    }
}
