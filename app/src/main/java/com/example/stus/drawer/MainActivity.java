package com.example.stus.drawer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.registerActivity(this);
        UiManager.getInstance().init(this);
        UiManager.showStartFragment();
    }

    @Override
    public void onBackPressed() {
        if (UiManager.getInstance().back()) {
            super.onBackPressed();
        }
    }
}
