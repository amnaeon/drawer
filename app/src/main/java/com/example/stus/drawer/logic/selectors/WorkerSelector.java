package com.example.stus.drawer.logic.selectors;


import com.example.stus.drawer.logic.workers.ClickWorker;
import com.example.stus.drawer.logic.workers.DragWorker;
import com.example.stus.drawer.logic.workers.IWork;

import static com.example.stus.drawer.logic.Const.Workers.ONE_STEP_WORKER;
import static com.example.stus.drawer.logic.Const.Workers.TWO_STEP_WORKER;

public class WorkerSelector {
    public static IWork getSelector(int type) {
        IWork iWork = null;
        switch (type){
            case ONE_STEP_WORKER:
                iWork = DragWorker.getInstance();
                break;
            case TWO_STEP_WORKER:
                iWork = new ClickWorker();
                break;
        }
        return iWork;
    }
}
