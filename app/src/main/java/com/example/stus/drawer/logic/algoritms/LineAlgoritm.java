package com.example.stus.drawer.logic.algoritms;


import com.example.stus.drawer.logic.models.Line;

public class LineAlgoritm<I extends Line, O extends Line> implements IAlgoritm<I, O> {
    private Line line;

    @Override
    public void prepare(I i) {
        line = (Line) i;
    }

    @Override
    public O getAlgoritmRes() {
        return (O) line;
    }
}
