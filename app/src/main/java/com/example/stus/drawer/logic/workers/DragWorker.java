package com.example.stus.drawer.logic.workers;


import com.example.stus.drawer.logic.algoritms.IAlgoritm;
import com.example.stus.drawer.logic.algoritms.TestAlgoritm;
import com.example.stus.drawer.logic.models.BaseModel;
import com.example.stus.drawer.logic.models.Point;

public class DragWorker implements IWork {
    private static DragWorker instance = null;

    private DragWorker(){}

    public static synchronized DragWorker getInstance() {
        if (instance == null)
            instance = new DragWorker();
        return instance;
    }

    @Override
    public IAlgoritm prepare(IAlgoritm iAlgoritm, BaseModel model) {
        if (iAlgoritm instanceof TestAlgoritm) {
            ((TestAlgoritm) iAlgoritm).prepare(new Point(model.getX(), model.getY()));
            return iAlgoritm;
        }

        return iAlgoritm;
    }

    @Override
    public IAlgoritm move(IAlgoritm iAlgoritm, BaseModel model) {
        return prepare(iAlgoritm, model);
    }

    @Override
    public IAlgoritm stop(IAlgoritm iAlgoritm, BaseModel model) {
        return prepare(iAlgoritm, model);
    }
}
