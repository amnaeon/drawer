package com.example.stus.drawer.logic;

import com.example.stus.drawer.logic.data_buffer.AlgoritmBuffer;

import java.util.ArrayList;
import java.util.List;

public class BackStack {
    private static List<AlgoritmBuffer> opperations = new ArrayList<>();

    private BackStack() {
    }

    private static BackStack instance = null;

    public static BackStack getInstance() {
        return instance == null ? new BackStack() : instance;
    }

    public void add(AlgoritmBuffer algoritmBuffer) {
        opperations.add(algoritmBuffer);
    }

    public List<AlgoritmBuffer> getOpperations() {
        return opperations;
    }
}
