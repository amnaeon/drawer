package com.example.stus.drawer.logic.data_buffer;


import com.example.stus.drawer.logic.algoritms.IAlgoritm;
import com.example.stus.drawer.logic.drawers.IDraw;

import java.util.ArrayList;
import java.util.List;

public class AlgoritmBuffer<P> {
    private List<P> algoritmDate = new ArrayList<>();
    private IDraw drawer = null;

    public void makeAction(IAlgoritm currentAlgoritm) {
        if (!currentAlgoritm.getAlgoritmRes().equals(null))
            algoritmDate.add((P) currentAlgoritm.getAlgoritmRes());
    }

    public List<P> getAlgoritmDate() {
        return algoritmDate;
    }

    public void setAlgoritmDate(List<P> algoritmDate) {
        this.algoritmDate = algoritmDate;
    }

    public IDraw getDrawer() {
        return drawer;
    }

    public void setDrawer(IDraw drawer) {
        this.drawer = drawer;
    }
}
