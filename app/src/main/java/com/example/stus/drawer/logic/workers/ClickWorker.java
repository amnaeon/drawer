package com.example.stus.drawer.logic.workers;


import com.example.stus.drawer.logic.algoritms.IAlgoritm;
import com.example.stus.drawer.logic.algoritms.LineAlgoritm;
import com.example.stus.drawer.logic.models.BaseModel;
import com.example.stus.drawer.logic.models.Line;
import com.example.stus.drawer.logic.models.Point;

/**
 * Created by stus on 23.06.17.
 */
public class ClickWorker implements IWork {
    private Point startCoordinate;


    @Override
    public IAlgoritm prepare(IAlgoritm iAlgoritm, BaseModel model) {
        if(iAlgoritm instanceof LineAlgoritm){
            ((LineAlgoritm) iAlgoritm).prepare(new Line("sd"));
            return iAlgoritm;
        }
        return iAlgoritm;
    }

    @Override
    public IAlgoritm move(IAlgoritm iAlgoritm,BaseModel model) {
        return iAlgoritm;
    }

    @Override
    public IAlgoritm stop(IAlgoritm iAlgoritm,BaseModel model) {
        return iAlgoritm;
    }
}
