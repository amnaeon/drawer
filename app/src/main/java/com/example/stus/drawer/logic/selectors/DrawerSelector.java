package com.example.stus.drawer.logic.selectors;

import com.example.stus.drawer.logic.drawers.DragDrawer;
import com.example.stus.drawer.logic.drawers.IDraw;
import static com.example.stus.drawer.logic.Const.Drawers.DRAG_DRAWER;

public class DrawerSelector {
    public static IDraw getDrawer(int type) {
        IDraw currentDrawer = null;
        switch (type) {
            case DRAG_DRAWER:
                currentDrawer= new DragDrawer();
                break;
        }
        return currentDrawer;
    }
}
